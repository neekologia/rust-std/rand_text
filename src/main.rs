use std::{thread, time, env};
use rand::{thread_rng, Rng};
use crossterm::terminal;

fn rand_az() -> char {
	let b = thread_rng().gen_bool(0.5);
	let a: u8 = {
		if b {
			thread_rng().gen_range(65..=90)
		} else {
			thread_rng().gen_range(97..=122)
		}
	};
	a as char
}

fn rand_punct() -> char {
	let n = thread_rng().gen_range(1..=3);
	let a: u8 = {
		match n {
			1 => thread_rng().gen_range(33..=47),
			2 => thread_rng().gen_range(91..=96),
			3 => thread_rng().gen_range(123..=126),
			_ => 0u8,
		}
	};
	a as char
}

fn rand_print() -> char {
	let b = thread_rng().gen_bool(0.22);
	let a: char = {
		if b {
			rand_punct()
		} else {
			rand_az()
		}
	};
	a
}

fn rand_s(cols: u16) -> String {
	let mut s = "".to_string();
	let max_s = thread_rng().gen_range(1..=cols);
	for _x in 1..=max_s {
		s = format!("{}{}", s, " ");
	}
	s
}

fn droplet() -> String {
	let (cols, _rows) = terminal::size().unwrap();
	let s = rand_s(cols).to_string();
	let d = format!("{}{}", s, rand_print());
	d
}

fn matrix() {
	let ms = time::Duration::from_millis(50);
	loop {
		println!("{}", droplet());
		thread::sleep(ms);
	}
}

fn warn() {
	println!("available arguments:\n- droplet\n- matrix");
}

fn main() {
	let flags: Vec<String> = env::args().collect();
	match flags.get(1) {
		Some(_) => (),
		None => {
			warn();
			return;
		}
	}
	match flags[1].as_str() {
		"droplet" => {
			println!("{}", droplet());
		}
		"matrix" => matrix(),
		_ => warn(),
	}
}
